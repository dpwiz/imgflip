{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}

module Imgflip
    ( getMemes
    , ImgflipResponse(..)
    , GetMemesItem(..)
    , captionImage
    ) where

import Prelude hiding (id)

import           Control.Lens    ((^.), (^?))
import           Data.Aeson      (FromJSON (..), ToJSON (..), object,
                                  withObject, (.:), (.=))
import           Data.Aeson.Lens (key, _Bool, _JSON, _String)
import           Data.Maybe      (fromMaybe)
import           Data.Text       (Text)
import           Network.Wreq    (FormParam ((:=)))
import qualified Network.Wreq    as Wreq

-- * JSON API

-- ** Common response envelope

data ImgflipResponse a = ImgflipResponse
    { success :: Bool
    , data'   :: Maybe a
    } deriving (Show)

-- ** get_memes

data GetMemesItem = GetMemesItem
    { id     :: Text
    , name   :: Text
    , url    :: Text
    , width  :: Int
    , height :: Int
    } deriving (Show)

instance FromJSON GetMemesItem where
    parseJSON = withObject "GetMemesItem" $ \o -> GetMemesItem
        <$> o .: "id"
        <*> o .: "name"
        <*> o .: "url"
        <*> o .: "width"
        <*> o .: "height"

instance ToJSON GetMemesItem where
    toJSON GetMemesItem{..} = object
        [ "id"     .= id
        , "name"   .= name
        , "url"    .= url
        , "width"  .= width
        , "height" .= height
        ]

getMemes :: IO (ImgflipResponse [GetMemesItem])
getMemes = do
    r <- Wreq.get "https://api.imgflip.com/get_memes"
    let body = r ^. Wreq.responseBody
    pure ImgflipResponse
        { success = fromMaybe False $ body ^? key "success" . _Bool
        , data' = body ^? key "data" . key "memes" . _JSON
        }

-- ** caption_image

captionImage
    :: (Text, Text)
    -> Text
    -> (Text, Text)
    -> IO (ImgflipResponse Text)
captionImage (username, password) templateId (text0, text1) = do
    r <- Wreq.post "https://api.imgflip.com/caption_image"
        [ "template_id" := templateId
        , "username" := username
        , "password" := password
        , "text0" := text0
        , "text1" := text1
        ]
    let body = r ^. Wreq.responseBody
    pure ImgflipResponse
        { success = fromMaybe False $ body ^? key "success" . _Bool
        , data' = body ^? key "data" . key "url" . _String
        }
