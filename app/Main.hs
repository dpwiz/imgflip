{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

import Prelude hiding (id)

import Data.Maybe         (fromMaybe)
import Data.Text          (pack)
import System.Environment (getEnv)

import Imgflip (GetMemesItem (..), ImgflipResponse (..), captionImage, getMemes)

main :: IO ()
main = do
  GetMemesItem{..} : _ <- fmap (fromMaybe [] . data') getMemes
  print (id, name)

  auth <- (,)
    <$> fmap pack (getEnv "IMGFLIP_USERNAME")
    <*> fmap pack (getEnv "IMGFLIP_PASSWORD")

  print =<< captionImage auth id
    ( "write API client with"
    , "wreq, aeson and lens"
    )
